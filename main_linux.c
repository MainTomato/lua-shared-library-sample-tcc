#include "main.h"

int sum(lua_State *L){
	int a = lua_tonumber(L, 1);
	int b = lua_tonumber(L, 2);
	lua_pushnumber(L, a + b);
	return 1;
}

int luaopen_libtest(lua_State *L) {
	lua_newtable(L);
	lua_pushstring(L, "sum"); lua_pushcfunction(L, sum); lua_rawset(L, -3);
	return 1;
}